import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:series_tracker/Scenes/Account/Login/Login.dart';
import 'package:series_tracker/Scenes/Shows/ShowDetails/ShowDetails.dart';

var accountRouteHandler = new Handler(
  handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return new LoginContainer();
  });

var showDetailsRouteHandler = new Handler(
  handlerFunc: (BuildContext context, Map<String, List<String>> params) {
    return new ShowDetailsContainer(showId: params["showId"].first);
  }
);