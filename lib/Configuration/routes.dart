import 'package:fluro/fluro.dart';
import 'package:flutter/widgets.dart';
import 'package:series_tracker/Configuration/route_handlers.dart';

class AppRoutes {
  static Router router = new Router();

  static String accountLogin = "/account/login";
  static String showDetails = "/show/details/";

  static void configureRoutes(Router router) {
    AppRoutes.router.define(accountLogin, handler: accountRouteHandler);
    AppRoutes.router.define(showDetails + ":showId", handler: showDetailsRouteHandler);
  }

  static void routeToAccountLogin(BuildContext context) {
    AppRoutes.router.navigateTo(context, AppRoutes.accountLogin);
  }

  static void routeToShowDetails(BuildContext context, String showId) {
    AppRoutes.router.navigateTo(context, AppRoutes.showDetails + showId);
  }

}
