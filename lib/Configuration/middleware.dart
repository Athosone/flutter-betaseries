import 'dart:io';

import 'package:redux/redux.dart';
import 'package:series_tracker/Common/main_app.dart';
import 'package:series_tracker/Scenes/Account/Data/AccountMiddleWare.dart';
import 'package:series_tracker/Scenes/Account/Infrastructure/BetaSeriesAccountRepository.dart';
import 'package:series_tracker/Scenes/Shows/Infrastructure/BetaSeriesShowRepository.dart';
import 'package:series_tracker/Scenes/Shows/Data/ShowsMiddleWare.dart';
import 'package:series_tracker/Scenes/Shows/Infrastructure/TVDBShowRepository.dart';

List<Middleware<SeriesAppState>> createAppMiddleware() {
  var accountMiddleware = createAccountMiddleware(new BetaSeriesAccountRepository(new HttpClient()));
  var seriesMiddleware = createShowMiddleware(new BetaSeriesShowRepository(new HttpClient()), new TVDBShowRepository(new HttpClient()));
  List<Middleware<SeriesAppState>> middleware = new List.from(accountMiddleware)..addAll(seriesMiddleware);
  return middleware;
}