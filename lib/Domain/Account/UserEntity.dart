import 'package:meta/meta.dart';

abstract class UserEntity {
  final String login;
  final String password;

  UserEntity({@required this.login, this.password});

  UserEntity copy();
}

@immutable
class BetaSeriesUser extends UserEntity {

  final String token;
  final String userId;

  BetaSeriesUser(String login, String password,
      {@required this.token, this.userId}): super(login: login, password: password);

  @override
  UserEntity copy() {
    return new BetaSeriesUser(login, password, token: token);
  }
}
