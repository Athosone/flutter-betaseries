import 'package:meta/meta.dart';
import 'package:series_tracker/Domain/ActorEntity.dart';

@immutable
class Notes {
  final String total;
  final String mean;

  const Notes(this.total, this.mean);
}

enum ShowImages { show, banner, box, poster }

class ShowEntity {
  final String id;
  final String thetvdbId;
  final String imdbId;
  final String name;
  final Map<ShowImages, Uri> images;
  final String creationYears;
  final String showDescription;
  final String status;
  final Notes notes;
  final List<String> genres;
  List<ActorEntity> actors;

  ShowEntity(
      this.id,
      this.thetvdbId,
      this.imdbId,
      this.name,
      this.images,
      this.creationYears,
      this.showDescription,
      this.status,
      this.notes,
      this.genres,
      {this.actors});

  static ShowEntity buildFromJSON(dynamic showMAP) {
    try {
      Map imagesHashMap = showMAP["images"];
      Map<ShowImages, Uri> images = {
        ShowImages.show: Uri.tryParse(imagesHashMap["show"]),
        ShowImages.banner: Uri.tryParse(imagesHashMap["banner"]),
        ShowImages.box: Uri.tryParse(imagesHashMap["box"]),
        ShowImages.poster: Uri.tryParse(imagesHashMap["poster"]),
      };

      var notes = new Notes(showMAP["notes"]["total"].toString(),
          showMAP["notes"]["mean"].toString());
      var status = showMAP["status"].toString();
      var showDescription = showMAP["description"];
      var creationYears = showMAP["creation"];
      List<String> genres = List<String>.from(showMAP["genres"]);
      String id = showMAP["id"].toString();
      return new ShowEntity(
          id,
          showMAP["thetvdb_id"].toString(),
          showMAP["imdb_id"],
          showMAP["title"],
          images,
          creationYears,
          showDescription,
          status,
          notes,
          genres);
    } catch (error) {
      return null;
    }
  }

  void updateActors(List<ActorEntity> actors) {
    this.actors = actors;
  }
}
