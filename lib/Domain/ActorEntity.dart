import 'package:meta/meta.dart';

@immutable
class ActorEntity {

  final String name;
  final String image;
  final String roleName;

  const ActorEntity(this.name, this.image, this.roleName);

  static ActorEntity buildFromJSON(dynamic actorMAP) {
    return new ActorEntity(actorMAP["actor"], actorMAP["picture"], actorMAP["name"]);
  }
}
