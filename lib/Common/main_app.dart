import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:series_tracker/Configuration/routes.dart';
import 'package:series_tracker/Configuration/middleware.dart';
import 'package:series_tracker/Scenes/Account/Data/AccountState.dart';
import 'package:series_tracker/Scenes/Account/Data/AccountReducer.dart';
import 'package:series_tracker/Scenes/RootScene.dart';
import 'package:series_tracker/Scenes/Shows/Data/ShowState.dart';
import 'package:series_tracker/Scenes/Shows/Data/ShowsReducer.dart';

class SeriesAppState {
  final bool isLoading;
  ShowState showState;
  AccountState accountState;

  SeriesAppState({this.accountState,
    this.showState,
    this.isLoading = false});

  factory SeriesAppState.loading() => new SeriesAppState(accountState: new AccountState(), showState: new ShowState(), isLoading: true);
}

SeriesAppState appReducer(SeriesAppState state, action) {
  return new SeriesAppState(
    accountState:  accountReducer(state.accountState, action),
    showState: showsReducer(state.showState, action),
  );
}

class App extends StatelessWidget {
  Store<SeriesAppState> store = new Store<SeriesAppState>(
    appReducer,
    initialState: new SeriesAppState.loading(),
    middleware: createAppMiddleware(),
  );

  App({Key key}): super(key: key) {
    AppRoutes.configureRoutes(AppRoutes.router);
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new StoreProvider<SeriesAppState>(store: store,
        child: new MaterialApp(
            title: 'Flutter Demo',
            theme: new ThemeData(
              primarySwatch: Colors.red,
              accentColor: Colors.red,
              buttonColor: Colors.blueAccent,
              indicatorColor: Colors.redAccent,
              textTheme: new TextTheme(
                title: new TextStyle(
                  color: Colors.white,
                  fontSize: 22.0,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.bold
                ),
                subhead: new TextStyle(
                    color: Colors.blueGrey,
                    fontSize: 18.0,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.normal
                ),
                body1: new TextStyle(
                    color: Colors.black,
                    fontSize: 16.0,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.normal,
                ),
                body2: new TextStyle(
                  color: Colors.black,
                  fontSize: 12.0,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.normal,
                ),
                button: new TextStyle(
                  color: Colors.blueAccent,
                )

              )
            ),
            home: new RootScene()
        )
    );
  }
}
