import 'dart:async';
import 'dart:io';

abstract class IRepository {
  Future<HttpClientRequest> buildPostRequest(String endPoint, Map<String, String> parameters);
  Future<HttpClientRequest> buildGetRequest(String endPoint, Map<String, String> parameters);
}

abstract class BaseBetaSeriesRepository implements IRepository {

  HttpClient _client;
  final String _host = "api.betaseries.com";
  //Clé API : 59f01ae76d3a
  final String _apiKey = "59f01ae76d3a";
  //Clé secrète (pour OAuth 2.0) : cff255ab7cb973fbe6fefca8c5e8cf79
  final String _secretKey = "cff255ab7cb973fbe6fefca8c5e8cf79";

  BaseBetaSeriesRepository(this._client);

  @override
  Future<HttpClientRequest> buildPostRequest(String endPoint, Map<String, String> parameters) async {
    var uri = new Uri.https(_host, endPoint, parameters);
    HttpClientRequest request = await _client.postUrl(uri);
    request.headers.add('X-BetaSeries-Key', _apiKey);
    return request;
  }

  @override
  Future<HttpClientRequest> buildGetRequest(String endPoint, Map<String, String> parameters) async {
    var uri = new Uri.https(_host, endPoint, parameters);
    HttpClientRequest request = await _client.getUrl(uri);
    request.headers.add('X-BetaSeries-Key', _apiKey);
    return request;
  }

}