import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';

@immutable
class CardMedia extends StatelessWidget {

  final String imageURI;
  final String title;
  final String subDescription;
  final String notes;
  final VoidCallback onTap;

  CardMedia({@required this.imageURI, this.title, this.subDescription, this.notes, this.onTap});

  @override
  Widget build(BuildContext context) {
    var textTheme = Theme.of(context).textTheme;
    var textTitleStyle = textTheme.title.apply(color: Colors.black, fontSizeFactor: 0.7);

    var imageRounded = new DecoratedBox(
      decoration: new BoxDecoration(
          image: new DecorationImage(
              image: new NetworkImage(imageURI),
              fit: BoxFit.cover
          ),
          shape: BoxShape.circle
      ),
    );
    var image = new ConstrainedBox(
        constraints: new BoxConstraints.expand(
            height: 100.0,
            width: 100.0),
        child: imageRounded
    );

    var centerImage =  new Center(
      child: new Padding(
          padding: new EdgeInsets.all(8.0),
          child: image
      ),
    );

    var flexibleTitle = new Flexible(
      child: new Text(
        title,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: textTitleStyle
      ),
    );
    var titleNotes = new Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        flexibleTitle,
        new Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Text(notes, style: textTitleStyle),
            new Icon(Icons.star, color: Colors.amber)
          ],
        )
      ],
    );

    var textDescription = new Text(
      subDescription,
      maxLines: 4,
      softWrap: true,
      overflow: TextOverflow.ellipsis,
      style: textTheme.body1,
    );

    var columnText = new Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        titleNotes,
        new Container(height: 4.0),
        textDescription,
      ],
    );
    var showInfos = new Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        centerImage,
        new Divider(height: 4.0),
        new Padding(
          padding: new EdgeInsets.only(left: 8.0, right: 8.0),
          child: columnText,
        )
      ],
    );
    var card = new Card(
      key: new Key(title + notes),
      child: showInfos,
      elevation: 6.0,
    );
    if (this.onTap != null) {
      var tap = new GestureDetector(
        child: card,
        onTap: this.onTap,
      );
      return tap;
    }
    return card;
  }

}