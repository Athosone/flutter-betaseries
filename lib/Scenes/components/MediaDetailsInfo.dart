import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MediaDetailsInfo extends StatefulWidget {
  final String title;
  final String subTitle;
  final String mediaDescription;

  MediaDetailsInfo(this.title, this.subTitle, this.mediaDescription);

  @override
  State<StatefulWidget> createState() => new _MediaDetailsInfoState(this.title, this.subTitle, this.mediaDescription);

}

class _MediaDetailsInfoState extends State<MediaDetailsInfo> {

  final String title;
  final String subTitle;
  final String mediaDescription;
  int _maxDescriptionLines = 4;
  String _maxDescriptionButtonText = "more...";

  _MediaDetailsInfoState(this.title, this.subTitle, this.mediaDescription);

  @override
  Widget build(BuildContext context) {
    var textTheme = Theme.of(context).textTheme;

    var mediasInfos = new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Padding(
          padding: new EdgeInsets.only(bottom: 8.0, top: 8.0),
          child: new Center(child: new Text(title, style: textTheme.title.apply(color: Colors.black))),
        ),
        new Padding(
          padding: new EdgeInsets.only(bottom: 12.0),
          child: new Text(subTitle, style: textTheme.subhead),
        ),
        new Padding(
          padding: new EdgeInsets.only(bottom: 12.0),
          child: new Text(mediaDescription, style: textTheme.body1, maxLines: _maxDescriptionLines, overflow: TextOverflow.ellipsis),
        ),
        new FlatButton(onPressed: _adjustMaxDescriptionLines, child: new Text(_maxDescriptionButtonText, style: textTheme.button))
      ],
    );
    return new Padding(
        padding: new EdgeInsets.only(left: 16.0, right: 16.0),
        child: mediasInfos
    );
  }

  void _adjustMaxDescriptionLines() {
    setState((){
      _maxDescriptionLines = _maxDescriptionLines == 4 ? 100 : 4;
      _maxDescriptionButtonText = _maxDescriptionLines == 4 ? "more..." : "less..";
    });
  }
}