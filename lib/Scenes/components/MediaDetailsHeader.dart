import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';
import 'package:series_tracker/Scenes/components/ArcBannerImage.dart';

class MediaDetailsHeaderDrawing {
  static const double POSTER_RATIO = 0.7;
  static const double POSTER_HEIGHT = 150.0;

  static const double RATING_DIMENSION = 100.0;

  // BANNER
  static const double BANNER_PROPORTION = 3.0;
  static const double BANNER_RATIO = 1.4;
  static const double SPEED_SCROLL = 0.3;

  // Size from parent view
  final Size sizeParent;

  // pixels returned by scroll metrics
  final double pixels;

  MediaDetailsHeaderDrawing({@required this.sizeParent, @required this.pixels});

  Size getBannerSize() {
    var bannerHeight = sizeParent.height / BANNER_PROPORTION - getHeightOffset() * 4;
    var bannerWidth = bannerHeight * BANNER_RATIO;
    return new Size(bannerWidth, bannerHeight);
  }

  double getHeightOffset() {
    double heightOffset = pixels > 0.0 ? 0.0 : pixels;

    return heightOffset;
  }

  double getTopOffset() {
    double topOffset = pixels > 0.0 ? pixels * SPEED_SCROLL  : pixels;

    return topOffset;
  }

  Size getPosterSize() {
    var posterWidth = POSTER_HEIGHT * POSTER_RATIO;
    return new Size(posterWidth, POSTER_HEIGHT);
  }

  double getCurrentProgress() {
    return 0.0;
  }

  Size getRatingSize() {
    return new Size(RATING_DIMENSION, RATING_DIMENSION);
  }

  double getPosterBottomOffset() {
    return getRatingSize().height / 1.8;
  }

  double getBottomPadding() {
    return  getRatingSize().height / 2;
  }

  double getRatingBottomOffset() {
    return 0.0;
  }
}

class MediaDetailsHeader extends StatelessWidget {

  final String bannerImageURL;
  final String posterImageURL;
  final String rating;
  final String ratingCount;

  ScrollMetrics scrollMetrics;

  MediaDetailsHeader(this.bannerImageURL, this.posterImageURL, this.rating, this.ratingCount, this.scrollMetrics);


  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    var pixels = 0.0;
    if (scrollMetrics != null) {
      pixels = scrollMetrics.pixels;
    }
    var drawing = new MediaDetailsHeaderDrawing(sizeParent: screenSize, pixels: pixels);
    var textTheme = Theme.of(context).textTheme;

    double topOffset = drawing.getTopOffset();
    Size bannerSize = drawing.getBannerSize();

    var bannerImage = new Material(
      child: new ArcBannerImage(this.bannerImageURL, bannerSize.width, bannerSize.height)

    );

    var posterSize = drawing.getPosterSize();

    var posterImage = new Material(
      borderRadius: new BorderRadius.circular(4.0),
      elevation: 2.0,
      child: new Image.network(
        this.posterImageURL,
        fit: BoxFit.cover,
        height: posterSize.height,
        width: posterSize.width,
      ),
    );

    var h = drawing.getCurrentProgress();

    var ratingBox = new DecoratedBox(
      decoration: new BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.blue,
      ),
      child: new Container(
        height: drawing.getRatingSize().height,
        width: drawing.getRatingSize().width,
        child: new Center(child: new Text(rating, style: textTheme.title.apply(color: Colors.white))),
      ),
    );

    var ratingText = ratingCount.isNotEmpty ? ratingCount + " people rated this" : "";
    var ratingView = new Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        ratingBox,
        new Text(ratingText, style: textTheme.subhead.apply(color: Colors.blueGrey))
      ],
    );

    var medias = new Stack(
      overflow: Overflow.visible,
      children: <Widget>[
        new Padding(
          padding: new EdgeInsets.only(bottom: drawing.getBottomPadding()),
          child: new Container(height: bannerSize.height),
        ),
        new Positioned(
          top: topOffset,
          left: 0.0,
          right: 0.0,
          child: bannerImage,
        ),
        new Positioned(
            bottom: drawing.getPosterBottomOffset(),
            left: 0.0,
            right: 0.0,
            child:  new Center(child: posterImage)
        ),
        new Positioned(
            bottom: drawing.getRatingBottomOffset(),
            left: 0.0,
            right: 0.0,
            child: ratingView
        )
      ],
    );
    return medias;
  }

}