import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';
import 'package:series_tracker/Scenes/components/MediaDetailsHeader.dart';
import 'package:series_tracker/Scenes/components/MediaDetailsInfo.dart';



class Actor {

  final String name;
  final String image;
  final String roleName;

  const Actor(this.name, this.image, this.roleName);
}

class DetailsMediaContent {

  final String bannerImageURL;
  final String posterImageURL;
  final String title;
  final String subTitle;
  final String mediaDescription;
  final String rating;
  final String ratingCount;
  final List<Actor> actors;

  DetailsMediaContent({@required this.bannerImageURL,
    @required this.posterImageURL,
    @required this.title,
    @required this.mediaDescription,
    @required this.rating,
    @required this.ratingCount,
    @required this.subTitle,
    this.actors});

}

class DetailsMedia extends StatefulWidget {

  final DetailsMediaContent content;

  DetailsMedia({@required this.content});

  @override
  _DetailsMediaState createState() => new _DetailsMediaState();
}

class _DetailsMediaState extends State<DetailsMedia> {

  ScrollMetrics metrics;
  
  _DetailsMediaState();
  
  @override
  Widget build(BuildContext context) {
    var content = widget.content;
    
    return new Scaffold(
        body: new NotificationListener<ScrollNotification>(
        onNotification: (ScrollNotification notification) {
          if (notification.metrics.axis == Axis.vertical) {
            setState(() => metrics = notification.metrics);
          }
          return true;
        },
        child: new SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: new Column(
             children: <Widget>[
               new MediaDetailsHeader(content.bannerImageURL, content.posterImageURL, content.rating, content.ratingCount, metrics),
               new MediaDetailsInfo(content.title, content.subTitle, content.mediaDescription),
               content.actors != null ? new ActorList(content.actors) : new Container(),
               new Container(height: 1000.0,)
             ],
            ),
          ),
        )
    );
  }

}

class SimpleCard extends StatelessWidget {

  final String image;
  final String title;

  SimpleCard(this.image, this.title);

  @override
  Widget build(BuildContext context) {
    var textTheme = Theme.of(context).textTheme;

    var posterImage = new Material(
      borderRadius: new BorderRadius.circular(4.0),
      elevation: 2.0,
      child: new Image.network(
        this.image,
        fit: BoxFit.cover,
        height: 150.0,
        width: 80.0
      ),
    );

    return new Column(
      children: <Widget>[
        new Padding(
          child: posterImage,
          padding: new EdgeInsets.only(left: 16.0, right: 16.0, bottom: 12.0),
        ),
        new Text(title, style: textTheme.body2),
      ],
    );
  }
}

class ActorList extends StatelessWidget {

  final List<Actor> actors;

  ActorList(this.actors);

  @override
  Widget build(BuildContext context) {

    List<SimpleCard> cards = actors.map((actor) {
        return new SimpleCard(actor.image, actor.name);
    }).toList();
    return new Padding(
        padding: new EdgeInsets.all(16.0),
        child: new SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: cards
            ),
        )
    );
  }
}

