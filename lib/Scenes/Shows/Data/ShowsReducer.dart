import 'package:series_tracker/Scenes/Shows/Data/ShowAction.dart';
import 'package:series_tracker/Scenes/Shows/Data/ShowState.dart';
import 'package:redux/redux.dart';


final showsReducer = combineReducers<ShowState>([
  new TypedReducer<ShowState, ShowsFetchedAction>(_fetchedShows),
  new TypedReducer<ShowState, ShowsFetchFailed>(_fetchFailedShows),
  new TypedReducer<ShowState, FetchShowsAction>(_fetchingShows),
  new TypedReducer<ShowState, FetchActorsAction>(_fetchShowActors),
  new TypedReducer<ShowState, ActorsFetchedAction>(_fetchedShowActors),
]);

ShowState _fetchedShows(ShowState prev, ShowsFetchedAction action) {
  return new ShowState(shows: action.shows, error: null, isLoading: false);
}

ShowState _fetchFailedShows(ShowState prev, ShowsFetchFailed action) {
  return new ShowState(shows: null, error: action.error, isLoading: false);
}

ShowState _fetchingShows(ShowState prev, FetchShowsAction action) {
  return new ShowState(shows: null, error: null, isLoading: true);
}

ShowState _fetchShowActors(ShowState prev, FetchActorsAction action) {
  return new ShowState(shows: prev.shows, error: null, isLoading: true);
}

ShowState _fetchedShowActors(ShowState prev, ActorsFetchedAction action) {
  var shows = prev.shows;
  var currentShow = shows.firstWhere((show) => show.id == action.showId);
  currentShow.updateActors(action.actors);
  return new ShowState(shows: shows, error: null, isLoading: false);
}