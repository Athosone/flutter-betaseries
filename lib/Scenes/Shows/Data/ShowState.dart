import 'package:series_tracker/Domain/Show/ShowEntity.dart';

class ShowState {

  List<ShowEntity> shows;
  String error;
  bool isLoading;

  ShowState({this.shows, this.error, this.isLoading = false});
}
