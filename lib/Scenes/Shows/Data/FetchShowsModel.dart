import 'package:meta/meta.dart';
import 'package:series_tracker/Domain/ActorEntity.dart';
import 'package:series_tracker/Domain/Show/ShowEntity.dart';

@immutable
class FetchShowsRequest {

  final bool orderByAsc;
  final String userId;
  final List<String> showIds;

  const FetchShowsRequest(this.orderByAsc, this.userId, this.showIds);
}

@immutable
class FetchShowActorsRequest {
  final String showId;

  const FetchShowActorsRequest(this.showId);
}


@immutable
class FetchShowActorsResponse {
  final List<ActorEntity> actors;

  const FetchShowActorsResponse(this.actors);
}

@immutable
class FetchShowsResponse {
  final List<ShowEntity> shows;

  const FetchShowsResponse(this.shows);
}
