import 'dart:async';

import 'package:series_tracker/Scenes/Shows/Data/FetchShowsModel.dart';

class FetchShowsError extends StateError {
  FetchShowsError(String message) : super(message);
}

abstract class IShowRepository {
  Future<FetchShowsResponse> fetchShows(FetchShowsRequest request);
  Future<FetchShowActorsResponse> fetchActors(FetchShowActorsRequest request);
}
