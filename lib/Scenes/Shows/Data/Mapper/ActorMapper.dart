import 'package:series_tracker/Domain/ActorEntity.dart';
import 'package:series_tracker/Scenes/components/DetailsMedia.dart';


class ActorMapper {

  static fromEntity(ActorEntity actorE) {
    return new Actor(actorE.name, actorE.image, actorE.roleName);
  }

}