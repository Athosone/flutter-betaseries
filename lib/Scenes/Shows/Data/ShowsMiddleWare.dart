import 'package:redux/redux.dart';
import 'package:series_tracker/Scenes/Shows/Data/IShowRespository.dart';
import 'package:series_tracker/Scenes/Shows/ListShows/ListShowsMiddleware.dart';
import 'package:series_tracker/Scenes/Shows/Data/ShowAction.dart';
import 'package:series_tracker/Scenes/Shows/ShowDetails/ShowDetailsMiddleware.dart';
import 'package:series_tracker/Common/main_app.dart';

// Middleware
List<Middleware<SeriesAppState>> createShowMiddleware(IShowRepository betaSeriesRepository, IShowRepository tvdbRepository) {
  final showBetaSeries = createListShowsMiddleware(betaSeriesRepository);
  final showTVDB = createShowDetailsMiddleware(tvdbRepository);
  return List<Middleware<SeriesAppState>>.of([
    new TypedMiddleware<SeriesAppState, FetchShowsAction>(showBetaSeries),
    new TypedMiddleware<SeriesAppState, FetchActorsAction>(showBetaSeries),
  ]);
}