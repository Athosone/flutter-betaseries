import 'package:flutter/foundation.dart';
import 'package:series_tracker/Configuration/action.dart';
import 'package:series_tracker/Domain/ActorEntity.dart';
import 'package:series_tracker/Domain/Show/ShowEntity.dart';
import 'package:series_tracker/Scenes/Shows/Data/FetchShowsModel.dart';

@immutable
class FetchShowsAction extends Action {
  final FetchShowsRequest request;

  FetchShowsAction({@required this.request});
}

@immutable
class ShowsFetchedAction extends Action {
  final List<ShowEntity> shows;

  ShowsFetchedAction({@required this.shows});
}

@immutable
class ShowsFetchFailed extends Action {
  final String error;

  ShowsFetchFailed({@required this.error});
}

@immutable
class FetchActorsAction extends Action {
  final FetchShowActorsRequest request;

  FetchActorsAction(this.request);
}

@immutable
class ActorsFetchedAction extends Action {
  final List<ActorEntity> actors;
  final String showId;

  ActorsFetchedAction(this.actors, this.showId);
}

