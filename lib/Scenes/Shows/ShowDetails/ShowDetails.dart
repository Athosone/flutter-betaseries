import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:meta/meta.dart';
import 'package:series_tracker/Common/main_app.dart';
import 'package:series_tracker/Domain/Show/ShowEntity.dart';
import 'package:series_tracker/Scenes/Shows/Data/FetchShowsModel.dart';
import 'package:series_tracker/Scenes/Shows/Data/Mapper/ActorMapper.dart';
import 'package:series_tracker/Scenes/Shows/Data/ShowAction.dart';
import 'package:series_tracker/Scenes/Shows/Data/ShowState.dart';
import 'package:series_tracker/Scenes/Shows/ShowDetails/ShowDetailsViewModel.dart';
import 'package:series_tracker/Scenes/components/DetailsMedia.dart';



class ShowDetailsContainer extends StatelessWidget {

  final String showId;

  ShowDetailsContainer({@required this.showId});

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<SeriesAppState, ShowDetailsViewModel>(converter: (store) {
        ShowState showState = store.state.showState;
        var show = showState.shows.firstWhere((show) => show.id == showId);
        if (show.actors == null) {
          var request = new FetchShowActorsRequest(show.id);
          store.dispatch(new FetchActorsAction(request));
        }
        return new ShowDetailsViewModel(
            bannerURL: show.images[ShowImages.show].toString(),
            posterURL: show.images[ShowImages.poster].toString(),
            notes: double.parse(show.notes.mean).ceil().toString(),
            notesCount: show.notes.total,
            name: show.name,
            showDescription: show.showDescription,
            genres: show.genres.join(","),
            showId: show.id,
            actors: show.actors != null ? show.actors.map(ActorMapper.fromEntity).toList() : null
        );
      },
      builder: (BuildContext context, viewModel) {
        return new ShowDetailsScreen(viewModel: viewModel);
      },
    );
  }
}

class ShowDetailsScreen extends StatelessWidget {

  final ShowDetailsViewModel viewModel;

  ShowDetailsScreen({@required this.viewModel});

  @override
  Widget build(BuildContext context) {
    return new DetailsMedia(
        content: new DetailsMediaContent(
          bannerImageURL: viewModel.bannerURL,
          posterImageURL: viewModel.posterURL,
          rating: viewModel.notes,
          ratingCount: viewModel.notesCount,
          title: viewModel.name,
          subTitle: viewModel.genres,
          mediaDescription: viewModel.showDescription,
          actors: viewModel.actors
        )
    );
  }

}
// new AppBar(
//elevation: 0.0,
//title: new Text(content.title),
//automaticallyImplyLeading: true),
