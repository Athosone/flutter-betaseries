import 'package:redux/redux.dart';
import 'package:series_tracker/Common/main_app.dart';
import 'package:series_tracker/Scenes/Shows/Data/IShowRespository.dart';
import 'package:series_tracker/Scenes/Shows/Data/ShowAction.dart';

Middleware<SeriesAppState> createShowDetailsMiddleware(IShowRepository repository) {
  return (Store store, action, NextDispatcher next) {
    print("FETCH ACTORS");
    next(action);
    repository.fetchShows(action.request).then((fetchResponse) {
      var show = fetchResponse.shows.first;
      store.dispatch(new ActorsFetchedAction(show.actors, show.thetvdbId));
    }).catchError((error) {
      store.dispatch(new ShowsFetchFailed(error: error.toString()));
    });
  };
}
