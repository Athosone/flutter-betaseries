import 'package:meta/meta.dart';
import 'package:series_tracker/Scenes/components/DetailsMedia.dart';


class ShowDetailsViewModel {

  final String showId;
  final String name;
  final String bannerURL;
  final String posterURL;
  final String showDescription;
  final String notes;
  final String notesCount;
  final String genres;
  final List<Actor> actors;

  ShowDetailsViewModel({@required this.showId,
    @required this.name,
    @required this.bannerURL,
    @required this.posterURL,
    @required this.showDescription,
    @required this.notes,
    @required this.notesCount,
    @required this.genres,
    this.actors});

}