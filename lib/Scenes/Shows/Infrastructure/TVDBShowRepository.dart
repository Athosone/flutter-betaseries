import 'dart:async';
import 'dart:io';

import 'package:series_tracker/Scenes/Shows/Data/FetchShowsModel.dart';
import 'package:series_tracker/Scenes/Shows/Data/IShowRespository.dart';


class TVDBShowRepository implements IShowRepository {

  HttpClient _client;

  TVDBShowRepository(this._client);

  @override
  Future<FetchShowsResponse> fetchShows(FetchShowsRequest showRequest) async {
    var parameters = {
      'id': showRequest.userId,
    };
    return new FetchShowsResponse(null);
  }

  @override
  Future<FetchShowActorsResponse> fetchActors(FetchShowActorsRequest request) {
    return null;
  }


}