import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:series_tracker/Common/Repository/Repository.dart';
import 'package:series_tracker/Domain/ActorEntity.dart';
import 'package:series_tracker/Domain/Show/ShowEntity.dart';
import 'package:series_tracker/Scenes/Shows/Data/FetchShowsModel.dart';
import 'package:series_tracker/Scenes/Shows/Data/IShowRespository.dart';

class BetaSeriesShowRepository extends BaseBetaSeriesRepository with IShowRepository {

  BetaSeriesShowRepository(HttpClient client) : super(client);

  @override
  Future<FetchShowsResponse> fetchShows(FetchShowsRequest showRequest) async {
    var parameters = {
      'id': showRequest.userId,
    };
    var request = await buildGetRequest('/shows/member', parameters);
    var response = await request.close();

    var responseBody = await response.transform(utf8.decoder).join();
    Map data = json.decode(responseBody);

    if (data["errors"].length > 0) {
      throw new FetchShowsError("An error occured, check your credentials");
    }
    List dataShows = data["shows"];
    List<ShowEntity> shows = dataShows.map(ShowEntity.buildFromJSON).toList();
    shows.removeWhere((show) => show == null);
    return new FetchShowsResponse(shows);
  }

  @override
  Future<FetchShowActorsResponse> fetchActors(FetchShowActorsRequest showRequest) async {
    var parameters = {
      'id': showRequest.showId,
    };
    var request = await buildGetRequest('/shows/characters', parameters);
    var response = await request.close();

    var responseBody = await response.transform(utf8.decoder).join();
    Map data = json.decode(responseBody);

    if (data["errors"].length > 0) {
      throw new FetchShowsError("An error occured, check your credentials");
    }
    List dataActors = data["characters"];
    List<ActorEntity> actors = dataActors.map(ActorEntity.buildFromJSON).toList();
    return new FetchShowActorsResponse(actors);
  }

}
