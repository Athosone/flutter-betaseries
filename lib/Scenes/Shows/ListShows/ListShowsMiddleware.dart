import 'package:redux/redux.dart';
import 'package:series_tracker/Common/main_app.dart';
import 'package:series_tracker/Scenes/Shows/Data/IShowRespository.dart';
import 'package:series_tracker/Scenes/Shows/Data/ShowAction.dart';

Middleware<SeriesAppState> createListShowsMiddleware(IShowRepository repository) {
  return (Store<SeriesAppState> store, action, NextDispatcher next) {
    print("LIST SHOWS BETASERIES");
    next(action);
    if (action is FetchActorsAction) {
      repository.fetchActors(action.request).then((fetchResponse) {
        store.dispatch(new ActorsFetchedAction(fetchResponse.actors, action.request.showId));
      }).catchError((error) {
        store.dispatch(new ShowsFetchFailed(error: error.toString()));
      });
      return;
    }

    repository.fetchShows(action.request).then((fetchResponse) {
      store.dispatch(new ShowsFetchedAction(shows: fetchResponse.shows));
    }).catchError((error) {
      store.dispatch(new ShowsFetchFailed(error: error.toString()));
    });
  };
}
