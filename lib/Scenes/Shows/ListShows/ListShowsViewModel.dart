import 'package:meta/meta.dart';


@immutable
class ShowViewModel {
  final String showId;
  final String name;
  final Uri imageURL;
  final String showDescription;
  String notes;

  ShowViewModel(String notes, {@required this.showId, @required this.name, @required this.imageURL, @required this.showDescription}) {
    this.notes = double.parse(notes).ceil().toString();
  }
}

@immutable
class ListShowsViewModel {

  final List<ShowViewModel> shows;

  ListShowsViewModel({this.shows});

}