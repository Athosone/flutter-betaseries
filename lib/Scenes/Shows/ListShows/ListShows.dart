import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:series_tracker/Common/keys.dart';
import 'package:series_tracker/Common/main_app.dart';
import 'package:series_tracker/Configuration/routes.dart';
import 'package:series_tracker/Domain/Account/UserEntity.dart';
import 'package:series_tracker/Domain/Show/ShowEntity.dart';
import 'package:series_tracker/Scenes/Account/Data/AccountState.dart';
import 'package:series_tracker/Scenes/Shows/Data/FetchShowsModel.dart';
import 'package:series_tracker/Scenes/Shows/Data/ShowAction.dart';
import 'package:series_tracker/Scenes/Shows/Data/ShowState.dart';
import 'package:series_tracker/Scenes/Shows/ListShows/ListShowsViewModel.dart';
import 'package:series_tracker/Scenes/components/CardMedia.dart';

class ListShowsContainer extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<SeriesAppState, ListShowsViewModel>(
        converter: (store) {
          ShowState showState = store.state.showState;
          AccountState accountState = store.state.accountState;
          BetaSeriesUser user = accountState.user;

          if (showState.shows == null && user != null && showState.isLoading == false && showState.error == null) {
            FetchShowsRequest request = new FetchShowsRequest(false, user.userId, null);
            store.dispatch(new FetchShowsAction(request: request));
          }
          if (showState.error != null) {

          }
          if (showState.shows != null) {
            var listShowsViewModel = new ListShowsViewModel(shows: showState.shows.map((show) {
              return new ShowViewModel(show.notes.mean,
                  showId: show.id,
                  name: show.name,
                  imageURL: show.images[ShowImages.show],
                  showDescription: show.showDescription);
            }).toList());
            return listShowsViewModel;
          }
          return new ListShowsViewModel();
        },
        builder: (context, viewModel) {
          return new Scaffold(
              appBar: new AppBar(
                  title: new Text("List shows"),
                  actions: <Widget>[
                    new IconButton(icon: new Icon(Icons.account_circle), onPressed: () => AppRoutes.routeToAccountLogin(context))
                  ]
              ),
              body: new ListShowsScreen(shows: viewModel.shows)
          );
        }
    );
  }
}

class ListShowsScreen extends StatelessWidget {

  final List<ShowViewModel> shows;

  ListShowsScreen({Key key, this.shows}): super(key: key ?? SeriesTrackerKeys.listShowsScreen);

  @override
  Widget build(BuildContext context) {
    if (shows != null) {
      var size = MediaQuery.of(context).size;
      var itemHeight = size.height / 3;
      var itemWidth = size.width / 2;
      var gridViewDelegate = new SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        childAspectRatio: itemWidth / itemHeight
      );

      var gridView = new GridView.builder(
          itemCount: shows.length,
          shrinkWrap: true,
          gridDelegate: gridViewDelegate,
          itemBuilder: (BuildContext context, int index) {
            if (index >= shows.length) {
              return null;
            }
            var showVM = shows[index];

            return new CardMedia(imageURI: showVM.imageURL.toString(),
                title: showVM.name,
                subDescription: showVM.showDescription,
                notes: showVM.notes,
                onTap: () => AppRoutes.routeToShowDetails(context, showVM.showId)
            );
          }
      );
      return new Container(
        padding: new EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
        child: new Column(
          children: <Widget>[
            new Expanded(
                child: gridView
            ),
          ],
        ),
      );
    }
    return new Center(child: new Text("Login first to see your favorite shows"));
  }

}