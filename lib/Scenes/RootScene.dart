import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:series_tracker/Scenes/Shows/ListShows/ListShows.dart';
import 'package:series_tracker/Scenes/Movies/ListMovies.dart';
import 'package:series_tracker/Scenes/Shows/Data/ShowsReducer.dart';

class RootScene extends StatefulWidget {
  @override
  RootSceneState createState() => new RootSceneState();

}

class RootSceneState extends State<RootScene> with SingleTickerProviderStateMixin {

  TabController controller;

  @override
  void initState() {
    super.initState();
    controller = new TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      bottomNavigationBar: new Material(
        color: Colors.amber,
        child: new TabBar(
          controller: controller,
          isScrollable: true,
          tabs: <Widget>[
            new Tab(
              text: "Shows",
              icon: new Icon(Icons.contacts),
            ),
            new Tab(
              text: "Movies",
              icon: new Icon(Icons.local_car_wash),
            ),
          ],
        ),
      ),
      body: new TabBarView(
        controller: controller,
        children: <Widget>[
          new ListShowsContainer(),
          new ListMovies()
        ],
      ),
    );
  }

}