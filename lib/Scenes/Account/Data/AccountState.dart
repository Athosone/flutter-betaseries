import 'package:series_tracker/Domain/Account/UserEntity.dart';


class AccountState {
  UserEntity user;
  String error;
  bool isLoading;

  AccountState({this.user, this.error, this.isLoading = false});

  AccountState copy() {
      UserEntity user = this.user.copy();
      return new AccountState(user: user, error: error, isLoading: isLoading);
  }
}
