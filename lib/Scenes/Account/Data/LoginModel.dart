import 'package:meta/meta.dart';
import 'package:series_tracker/Domain/Account/UserEntity.dart';

@immutable
class LoginRequest {
  final String login;
  final String password;

  const LoginRequest(this.login, this.password);
}

@immutable
class LoginResponse {
  final UserEntity user;

  LoginResponse({this.user});
}