import 'package:meta/meta.dart';
import 'package:series_tracker/Configuration/action.dart';
import 'package:series_tracker/Domain/Account/UserEntity.dart';

abstract class AccountAction extends Action {}

@immutable
class BetaSeriesUserLoadedAction extends AccountAction  {
  final BetaSeriesUser user;

  BetaSeriesUserLoadedAction({@required this.user});
}

@immutable
class UserLoadingFailedAction extends AccountAction  {
  final String error;

  UserLoadingFailedAction({@required this.error});
}