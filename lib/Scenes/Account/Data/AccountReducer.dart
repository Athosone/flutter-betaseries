import 'package:redux/redux.dart';
import 'package:series_tracker/Scenes/Account/Data/AccountAction.dart';
import 'package:series_tracker/Scenes/Account/Data/AccountState.dart';
import 'package:series_tracker/Scenes/Account/Login/LoginAction.dart';

final accountReducer = combineReducers<AccountState>([
  new TypedReducer<AccountState, BetaSeriesUserLoadedAction>(_userLoaded),
  new TypedReducer<AccountState, UserLoadingFailedAction>(_userLoadingFailed),
  new TypedReducer<AccountState, LoginBetaSeriesUserAction>(_userLogin)
]);

AccountState _userLoadingFailed(AccountState prev, UserLoadingFailedAction action) {
  return new AccountState(user: null, error: action.error, isLoading: false);
}

AccountState _userLoaded(AccountState prev, BetaSeriesUserLoadedAction action) {
  return new AccountState(user: action.user, error: null, isLoading: false);
}

AccountState _userLogin(AccountState prev, LoginBetaSeriesUserAction action) {
  return new AccountState(user: null, error: null, isLoading: true);
}

