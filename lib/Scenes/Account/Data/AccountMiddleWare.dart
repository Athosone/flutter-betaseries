import 'package:redux/redux.dart';
import 'package:series_tracker/Scenes/Account/Data/IAccountRepository.dart';
import 'package:series_tracker/Scenes/Account/Login/LoginAction.dart';
import 'package:series_tracker/Scenes/Account/Login/LoginMiddleware.dart';
import 'package:series_tracker/Common/main_app.dart';

// Middleware
List<Middleware<SeriesAppState>> createAccountMiddleware(IAccountRepository repository) {
  final loginUser = createLoginBetaSeriesUser(repository);

  return new List<Middleware<SeriesAppState>>.of([
    new TypedMiddleware<SeriesAppState, LoginBetaSeriesUserAction>(loginUser),
  ]);
}

