import 'dart:async';
import 'package:series_tracker/Scenes/Account/Data/LoginModel.dart';

class AccountLoginError extends StateError {
  AccountLoginError(String message) : super(message);
}

abstract class IAccountRepository {
  Future<LoginResponse> loginUser(LoginRequest request);
}

