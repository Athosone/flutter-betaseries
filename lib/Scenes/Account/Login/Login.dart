import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:meta/meta.dart';
import 'package:series_tracker/Common/keys.dart';
import 'package:async_loader/async_loader.dart';
import 'package:series_tracker/Common/main_app.dart';
import 'package:series_tracker/Scenes/Account/Data/LoginModel.dart';
import 'package:series_tracker/Scenes/Account/Login/LoginAction.dart';
import 'package:series_tracker/Scenes/Account/Login/LoginViewModel.dart';

class LoginContainer extends StatelessWidget {

  LoginContainer({Key key}): super(key: key);


  @override
  Widget build(BuildContext context) {
    return new StoreConnector<SeriesAppState, LoginViewModel>(
      converter: (store) {
        OnSaveCallBack onSave = (login, password) {
          LoginRequest request = new LoginRequest(login, "c8PfYNgqiscE");
          store.dispatch(new LoginBetaSeriesUserAction(request: request));
        };
        var isLoading = store.state.accountState.isLoading;
        var isLogged = store.state.accountState.user != null;
        var error = store.state.accountState.error;
        return new LoginViewModel(onSave: onSave, error: error, isLoading: isLoading, isLogged: isLogged);
      },
      builder: (BuildContext context, viewModel) {
        return new Scaffold(
          body: new Container(
            decoration: new BoxDecoration(
              image: new DecorationImage(
                  image: new AssetImage("tvShows.jpg"),
                  fit: BoxFit.cover
              ),
            ),
            child: new LoginScreen(
                isLoading: viewModel.isLoading,
                onSave: viewModel.onSave,
                isLogged: viewModel.isLogged
            ),
          ),
        );
      },
    );
  }
}

@immutable
class LoginScreen extends StatelessWidget {
  static final formKey = new GlobalKey<FormState>();
  static final formFieldUsernameStateKey = new GlobalKey<FormFieldState<String>>();
  static final formFieldPasswordStateKey = new GlobalKey<FormFieldState<String>>();

  final GlobalKey<AsyncLoaderState> _asyncLoaderState = new GlobalKey<AsyncLoaderState>();


  final bool isLoading;
  final bool isLogged;
  final OnSaveCallBack onSave;

  final TextStyle textStyle = new TextStyle(decorationColor: Colors.white, color: Colors.white);
  final inputTextStyle = new TextStyle(color: Colors.white.withAlpha(200));


  LoginScreen({Key key,
    @required this.onSave,
  @required this.isLoading,
  @required this.isLogged}): super(key: key ?? SeriesTrackerKeys.accountScreen);

  String _username;
  String _password;

  @override
  Widget build(BuildContext context) {
    if (isLogged) {
      return _buildUserLoaded(context);
    }
    return isLoading ? _buildAsync() : _buildWidget();
  }

  Widget _buildUserLoaded(BuildContext context) {

    return new Center(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text('Successfully logged in', style: textStyle),
                  new Container(height: 8.0),
                  new RaisedButton(onPressed: () => Navigator.pop(context),
                    child: new Text('Back'),
                  )
                ],
            ),
          );
  }

  Material _buildAsync() {
    return new Material(
      color: Colors.white,
      child: new Center(
        child: new CircularProgressIndicator()
      )
    );
  }

  Widget _buildWidget() {
    var formColumn =  new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new ListTile(
            leading: const Icon(Icons.person, color: Colors.white),
            title: new TextFormField(
              key: formFieldUsernameStateKey,
              decoration: new InputDecoration(hintText: "Login", hintStyle: inputTextStyle),
              validator: (val) =>
              val.isEmpty
                  ? 'Login can\'t be empty'
                  : null,
              style: textStyle,
              onSaved: (val) => _username = val,
            )),
        new ListTile(
            leading: const Icon(Icons.vpn_key, color: Colors.white),
            title: new TextFormField(
              key: formFieldPasswordStateKey,
              decoration: new InputDecoration(hintText: "Password", hintStyle: inputTextStyle),
              validator: (val) =>
              val.isEmpty
                  ? 'Password can\'t be empty'
                  : null,
              onSaved: (val) => _password = val,
              style: textStyle,
              obscureText: true,
            )),
        new RaisedButton(
            onPressed: _loginIsValid, child: new Text("Login"))
      ],
    );
    var form = new Form(key: formKey, child: formColumn);
    var lRet = new Container(
        color: Colors.black.withAlpha(150),
        child: form,
    );
    return lRet;
  }

  void _loginIsValid() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      this.onSave(_username, _password);
    }
  }
}
