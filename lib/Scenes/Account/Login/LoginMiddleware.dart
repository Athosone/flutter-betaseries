import 'package:redux/redux.dart';
import 'package:series_tracker/Scenes/Account/Data/AccountAction.dart';
import 'package:series_tracker/Scenes/Account/Data/IAccountRepository.dart';
import 'package:series_tracker/Common/main_app.dart';

Middleware<SeriesAppState> createLoginBetaSeriesUser(IAccountRepository repository) {
  return (Store<SeriesAppState> store, action, NextDispatcher next) {
    print("LOGIN BETASERIES");
    next(action);
    repository.loginUser(action.request).then((loginResponse) {
      store.dispatch(new BetaSeriesUserLoadedAction(user: loginResponse.user));
    }).catchError((error) {
      store.dispatch(new UserLoadingFailedAction(error: error.toString()));
    });
  };
}
