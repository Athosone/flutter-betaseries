import 'package:meta/meta.dart';
import 'package:series_tracker/Scenes/Account/Data/AccountAction.dart';
import 'package:series_tracker/Scenes/Account/Data/LoginModel.dart';

@immutable
class LoginBetaSeriesUserAction extends AccountAction  {

  final LoginRequest request;

  LoginBetaSeriesUserAction({@required this.request});
}
