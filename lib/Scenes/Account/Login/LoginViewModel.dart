import 'package:meta/meta.dart';

typedef OnSaveCallBack = Function(String, String);

class LoginViewModel {
  final String error;
  final bool isLoading;
  final OnSaveCallBack onSave;
  final bool isLogged;

  LoginViewModel({@required this.onSave, @required this.error, @required this.isLoading = false, @required this.isLogged});
}
