import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:series_tracker/Common/Repository/Repository.dart';
import 'package:series_tracker/Common/md5.dart';
import 'package:series_tracker/Domain/Account/UserEntity.dart';
import 'package:series_tracker/Scenes/Account/Data/IAccountRepository.dart';
import 'package:series_tracker/Scenes/Account/Data/LoginModel.dart';

class BetaSeriesAccountRepository extends BaseBetaSeriesRepository with IAccountRepository {

  BetaSeriesAccountRepository(HttpClient client): super(client);

  @override
  Future<LoginResponse> loginUser(LoginRequest loginRequest) async {
    var passwordMD5 = generateMD5(loginRequest.password);
    var request = await buildPostRequest('/members/auth', {'login': loginRequest.login, 'password': passwordMD5});
    var response = await request.close();
    var responseBody = await response.transform(utf8.decoder).join();
    Map data = json.decode(responseBody);
    print("LOGIN Response received");
    if (data["errors"].length > 0) {
      throw new AccountLoginError("An error occured, check your credentials");
    }
    BetaSeriesUser user = new BetaSeriesUser(data["user"]["login"], passwordMD5, userId: data["user"]["id"].toString(), token: data["token"]);
    var loginResponse = new LoginResponse(user: user);
    return loginResponse;
  }

}